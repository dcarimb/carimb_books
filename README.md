#Progetto Biblioteca
##Documentazione

### Query di creazione delle tabelle

```sql
show databases;
create databases e5_calzetta_books;
use e5_calzetta_books
show tables;
desc books;
```

```sql
create table books(
    title char(255),
    genre char(255),
    year int,
    price float
);
```
```sql
create table genres(
    genre char(60),
    description text
    
);
```

```sql
alter table books drop column price;
alter table books add column price float;
alter table genres add id int primary key  auto_increment first;
alter table books modify column price float;
alter table books change price prize float;
alter table books change prize price decimal(6,2);

```
#Inserire ed eliminare elementi nella tabella
```sql
insert into genres (genre, description)
values("fantasy", "Genere di tipo fntasy");

Delete from genres where genre = 'fantasy';

```
#Visualizzare gli elementi nell tabella
```sql
select * from genres;
```


### Query di aggiornamento 
```sql
update genres
set 
    description = 'Descrizione ';
where genre= 'fantasy';
```