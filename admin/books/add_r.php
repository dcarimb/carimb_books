<?php

require "../../config.php";

#var_export($_POST); die;

$title = $_POST['title'] ?? '';
$genre_id = intval($_POST['genre_id'] ?? null);
$year = $_POST['year'] ?? null;
$price = $_POST['price'] ?? null;
$authors_id = $_POST['authors_id'] ?? [];

if ($year == '') $year = null;
if ($price == '') $price = null;
//var_dump($year);

if ($title == '' || $genre_id == null) {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'title' => $title ,
        'genre_id' => $genre_id,
        'year' => $year ,
        'price' => $price,
        'authors_id' => $authors_id
    ];
    header('location: /admin/books/add.php?');
    die;
}


try {
    $stmt = $db-> prepare("
    INSERT INTO books SET 
    title = :title,
    genre_id = :genre_id,
    `year` = :year,
    price = :price
    ");

    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':genre_id', $genre_id);
    $stmt->bindParam(':year', $year);
    $stmt->bindParam(':price', $price);
    #$stmt->bindValue(':id', $id); //Fa dei controlli minori
    $stmt->execute();


    $book_id = $db->lastInsertId();
    $stmt = $db-> prepare("
        INSERT INTO authors_books VALUES (
            :author_id, :book_id
        )
    ");

    $stmt->bindParam(':book_id', $book_id);
    foreach ($authors_id as $author_id) {
        $stmt->bindParam(':author_id', $author_id, PDO::PARAM_INT);
        $stmt->execute();
    }


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/books/index.php');




?>



