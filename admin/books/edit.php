<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Modifica libro</title>
    <style>
        label {
            width: 4pc;
            display: inline-block;
        }

        input[value="Salva"] {
            background-color: yellowgreen;

        }
        
        input[type=submit], input[type=button], input[type=reset] {
            cursor: pointer;
            border: 1px solid #4444;
            border-radius: 2px;
        }
    </style>
</head>
<body>

<?php
require "../../config.php";

$id = intval($_GET['id']) ?? 0;

try {

    $stmtb = $db->prepare("SELECT * FROM books WHERE id = :id");
    $stmtb->bindParam(":id", $id);
    $stmtb->execute();
    $book = $stmtb->fetch(PDO::FETCH_ASSOC);

    $stmtab = $db->prepare("SELECT * FROM authors_books WHERE book_id = :id");
    $stmtab->bindParam(":id", $id);
    $stmtab->execute();
    $authors_id = [];
    while ($ab = $stmtab->fetch(PDO::FETCH_ASSOC)) {
        $authors_id[] = $ab['author_id'];
    }


    $stmt = $db-> prepare("SELECT * FROM genres");
    $stmt->execute();

    $stmta = $db-> prepare("SELECT * FROM authors");
    $stmta->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $title = $_SESSION['add_data']['title'];
    $msg = $_SESSION['add_data']['msg'];
    $genre_id= $_SESSION['add_data']['genre_id'];
    $year = $_SESSION['add_data']['year'];
    $price = $_SESSION['add_data']['price'];
    $authors_id = $_SESSION['add_data']['author_id'];
    unset($_SESSION['add_data']);
} else {
    $msg = '';
    $title = $book['title'];
    $genre_id = $book['genre_id'];
    $year =  $book['year'];
    $price = $book['price'];
}
?>

<h2>Modifica libro</h2>

<?php if($msg != ''): ?>
    <div class="error"><?= $msg?> </div>
<?php endif ?>
<br>

<form method="post" action="edit_r.php" enctype="multipart/form-data">
    <label for="title">Title</label>
    <input id="title" type="text" name="title" size="60" maxlength="255" value="<?= $title ?>">
    <br><br>


    <label for="genre_id">Genere</label>
    <select name="genre_id" id="genre_id">
        <option>-- Seleziona un genere --</option>
        <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
            <?php $selcd = ($row['id'] == $genre_id) ? 'selected' : '' ?>
            <option value="<?= $row['id'] ?>" <?= $selcd ?> ><?= $row['genre'] ?></option>
        <?php endwhile ?>
    </select>
    <br><br>

    <label for="authors">authors</label>
    <select name="authors_id[]" id="authors" multiple>
        <option>-- Seleziona gli autori --</option>
        <?php while($row = $stmta->fetch(PDO::FETCH_ASSOC)): ?>
            <option <?= in_array($row['id'], $authors_id) ? 'selected' : '' ?> value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
        <?php endwhile ?>
    </select>
    <br><br>

    <label for="year">Anno</label>
    <input id="year" type="number" name="year" size="6" maxlength="4" value="<?= $year ?>">
    <br><br>

    <label for="price">Prezzo</label>
    <input id="price" type="number" step="0.01" min="0" name="price" size="6" maxlength="10" value="<?= $price ?>">
    <br><br>

    <label for="image">Picture</label>
    <input id="image" type="file" name="image">
    <br><br>

    <input hidden id="id" name="id" type="number" value="<?= $id ?>">

    <input type="button" value="Annulla" onclick="history.back()">
    <input type="reset">
    <input type="submit" value="Salva">


</form>

</body>
</html>