<?php

require "../../config.php";

#echo '<pre>'; var_export($_POST); var_export($_FILES); die;

$title = $_POST['title'] ?? '';
$genre_id = intval($_POST['genre_id'] ?? null);
$year = $_POST['year'] ?? null;
$price = $_POST['price'] ?? null;
$authors_id = $_POST['authors_id'] ?? [];
$id = $_POST['id'] ?? 0;

if ($year == '') $year = null;
if ($price == '') $price = null;
if ($id == '') $id = 0;
//var_dump($year);

if ($title == '' || $genre_id == null) {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'msg' => 'Some required data is missing',
        'id' => $id,
        'title' => $title ,
        'genre_id' => $genre_id,
        'year' => $year ,
        'price' => $price,
        'authors_id' => $authors_id
    ];
    header('location: /admin/books/edit.php?id=' . $id);
    die;
}


try {

    $stmt = $db-> prepare("
       UPDATE books SET
        title = :title,
        genre_id = :genre_id,
        year = :year,
        price = :price
       
        where id = :book_id
    ");

    $stmt->bindParam(':title', $title);
    $stmt->bindParam(':book_id', $id);
    $stmt->bindParam(':genre_id', $genre_id);
    $stmt->bindParam(':year', $year);
    $stmt->bindParam(':price', $price);
    $stmt->execute();

    $stmt = $db-> prepare("
        DELETE FROM authors_books where book_id = :book_id
    ");
    $stmt->bindParam(':book_id', $id);
    $stmt->execute();


    $stmt = $db-> prepare("
        INSERT INTO authors_books VALUES (
            :author_id, :book_id
        )
    ");

    $stmt->bindParam(':book_id', $id);
    foreach ($authors_id as $author_id) {
        $stmt->bindParam(':author_id', $author_id, PDO::PARAM_INT);
        $stmt->execute();
    }


    # SAVE THE PICTURE

    if (isset($_FILES['image']) and $_FILES['image']['error'] == 0) {
        move_uploaded_file($_FILES['image']['tmp_name'], "../../pictures/$id.png");
    }


    }catch (PDOException $e) {
        echo "Errore: " . $e->getMessage();
        die();
    }

header('location: /admin/books/index.php');




?>



