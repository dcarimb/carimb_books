<?php
    require_once "../../config.php";
    require_once "../../authorised.php";
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco libri</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/admin.css">
</head>
<body>
<?php
require "../../config.php";

try {
    $stmt = $db -> prepare("
    SELECT B.id, B.title, B.year, B.price, G.genre,
           GROUP_CONCAT(A.name SEPARATOR ', ') AS authors     
    FROM books B
        LEFT JOIN genres G on B.genre_id = G.id
        LEFT JOIN authors_books AB ON B.id = AB.book_id
        LEFT JOIN authors A ON AB.author_id = A.id
    GROUP BY B.id
    ");
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<h1>Elenco libri</h1>
<a href="add.php"><span class="material-icons">add_circle_outline</span></a>
<br><br>

<table>
    <tr>
        <th>id</th>
        <th></th>
        <th>titolo</th>
        <th>Autori</th>
        <th>genre</th>
        <th>anno</th>
        <th>prezzo</th>
        <th></th>
    </tr>
    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td style="padding: 0px; text-align: center; vertical-align: middle">
                <?php if(file_exists("../../pictures/$row[id].png")): ?>
                    <img src="../../pictures/<?= $row['id']?>.png" width="32px">
                <?php else: ?>
                    <img src="../../pictures/0.png" width="32px">
                <?php endif ?>
            </td>
            <td><?= $row['authors'] ?></td>
            <td><?= $row['genre'] ?></td>
            <td><?= $row['year'] ?></td>
            <td><?= $row['price'] ?></td>
            <td>
                <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons">edit</span></button>
                <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons">delete</span></button>
            </td>

        </tr>
    <?php endwhile ?>
</table>

<br><br>

immagini, scheda libro, login admin, inserire autori generi

<script>
    function del(id) {
        if (confirm('Sei sicuro si voler eliminare questo libro?')) {
            location = "/admin/books/del.php?id=" + id
        }
    }

    function mod(id) {
        location = "/admin/books/edit.php?id=" + id;
    }
</script>

</body>
</html>