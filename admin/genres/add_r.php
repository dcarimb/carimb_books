<?php

require "../../config.php";

#var_export($_POST); die;

$genre = $_POST['genre'] ?? '';
$description = $_POST['description'] ?? '';


try {
    $stmt = $db-> prepare("
    INSERT INTO genres SET 
    genre = :genre,
    description = :description
    ");

    $stmt->bindParam(':genre', $genre);
    $stmt->bindParam(':description', $description);
    $stmt->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/genres/index.php');

?>



