<?php

$id = $_GET['id'] ?? 0;


header('location: /admin/genres/index.php');

require "../../config.php";

try {
    $stmt = $db-> prepare("DELETE FROM genres where id  = :id");
    $stmt->bindParam(':id', $id);
    #$stmt->bindValue(':id', $id); //Fa dei controlli minori
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}


?>