<!doctype html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../assets/admin.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Modifica libro</title>
    <style>
        label {
            width: 4pc;
            display: inline-block;
        }

        input[value="Salva"] {
            background-color: yellowgreen;

        }
        
        input[type=submit], input[type=button], input[type=reset] {
            cursor: pointer;
            border: 1px solid #4444;
            border-radius: 2px;
        }
    </style>
</head>
<body>

<?php
require "../../config.php";

$id = intval($_GET['id']) ?? 0;

try {

    $stmtb = $db->prepare("SELECT * FROM genres WHERE id = :id");
    $stmtb->bindParam(":id", $id);
    $stmtb->execute();
    $genres = $stmtb->fetch(PDO::FETCH_ASSOC);

    $stmta = $db-> prepare("SELECT * FROM authors");
    $stmta->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

if (isset($_SESSION['add_data'])) {
    $id = $_SESSION['add_data']['id'];
    $genre = $_SESSION['add_data']['genre'];
    $description= $_SESSION['add_data']['description'];
    unset($_SESSION['add_data']);
} else {
    $id = $genres['id'];
    $genre = $genres['genre'];
    $description =  $genres['description'];
}
?>

<h2>Modifica genere</h2>

<br>

<form method="post" action="edit_r.php" enctype="multipart/form-data">

    <label for="genre">genre</label> <br>
    <input id="genre" name="genre" size="20" maxlength="15" value="<?= $genre ?>"> <br><br>

    <label for="description">descrizione</label> <br>
    <input id="description" name="description" size="50" maxlength="60" value="<?= $description ?>"> <br><br>

    <input hidden id="id" name="id" type="number" value="<?= $id ?>">

    <input type="button" value="Annulla" onclick="history.back()">
    <input type="reset">
    <input type="submit" value="Salva">


</form>

</body>
</html>