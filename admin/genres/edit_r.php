<?php

require "../../config.php";

#var_export($_POST); die;

$genre = $_POST['genre'] ?? '';
$description = $_POST['description'] ?? '';

if ($genre == '' || $description == null) {
    # --> restituire messaggio di errore
    $_SESSION['add_data'] =  [
        'genre' => $genre,
        'description' => $description ,
    ];
    header('location: /admin/genres/edit.php?id=');
    die;
}


try {

    $stmt = $db-> prepare("
       UPDATE genres SET
        genre = :genre,
        description = :description,
       WHERE id = :id
    ");

    $stmt->bindParam(':genre', $genre);
    $stmt->bindParam(':description', $description);
    $stmt->execute();


}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

header('location: /admin/genres/index.php');




?>



