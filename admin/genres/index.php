<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco generi</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/admin.css">
</head>
<body>
<?php
require "../../config.php";

try {
    $stmt = $db -> prepare("
    SELECT * FROM genres
    ");
    $stmt->execute();
}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>
<h1>Elenco generi</h1>
<a href="add.php"><span class="material-icons">add_circle_outline</span></a>
<br><br>

<table>
    <tr>
        <th>id</th>
        <th>genre</th>
        <th>description</th>
        <th></th>
    </tr>
    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['genre'] ?></td>
            <td><?= $row['description'] ?></td>
            <td>
                <button onclick="mod(<?= $row['id'] ?>)"><span class="material-icons">edit</span></button>
                <button onclick="del(<?= $row['id'] ?>)"><span class="material-icons">delete</span></button>
            </td>

        </tr>
    <?php endwhile ?>
</table>

<br><br>

immagini, scheda libro, login admin, inserire autori generi

<script>
    function del(id) {
        if (confirm('Sei sicuro si voler eliminare questo genere?')) {
            location = "/admin/genres/del.php?id=" + id
        }
    }

    function mod(id) {
        location = "/admin/genres/edit.php?id=" + id;
    }
</script>

</body>
</html>