<?php

    require_once "../config.php";

    var_export($_POST);

    $username = $_POST['username'] ?? '';
    $password = $_POST['password'] ?? '';

    unset($_SESSION['user']);

try {
    $stmt = $db->prepare("
        SELECT id, username FROM users
        WHERE username=:username AND 'password'=MD5(CONCAT(:password; salt))
    ");
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', $password);
    $stmt->bindParam(':salt', $securitysalt);
    $stmt->execute();
    if ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $_SESSION['user']['role'] = $user;
    } else {
        $_SESSION['msg'] = "non sei abilitato all'accesso";
    }
} catch (PDOException $e) {
    echo "errore: ". $e->getMessage();
    die();
}

$backto = $_SESSION['backto'] ?? '/index.php';
unset($_SESSION['backto']); // consumo il gettone di ritorno

//echo $backto;
header("location: $backto");