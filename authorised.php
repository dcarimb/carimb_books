<?php

require_once "config.php";

if (($_SESSION['user']['role'] ?? '') != 'admin') {
    #echo "qualcosa";
    header('location: /admin/login.php'); # funziona sul client, mentre io devo lavorare sul server
    $_SESSION['backto'] = $_SERVER['REQUEST_URI'];
    die;
}

?>