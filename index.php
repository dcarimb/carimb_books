<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <title>Elenco libri</title>
    <link rel="stylesheet" href="assets/public.css">
    <link rel="icon" type="image/png" href="/assets/favicon-96x96.png">


</head>
<body>

<img src="assets/favicon-96x96.png" alt="Mia Immagine" style=" width: 40px; position: absolute;">

<?php
require "config.php";

$q = $_GET['q'] ?? '';

try {
    $sql = "
        SELECT B.id, B.title, B.year, B.price, G.genre,
             GROUP_CONCAT(A.name SEPARATOR ', ') AS authors
        FROM books B
          LEFT JOIN genres G ON B.genre_id = G.id
          LEFT JOIN authors_books AB ON B.id = AB.book_id
          LEFT JOIN authors A ON AB.author_id = A.id
    ";

    $sql .= "GROUP BY B.id";
    if ($q != ''){
        $sql .= "HAVING B.title LIKE :q OR authors LIKE :q
            OR authors LIKE :q
            OR genre LIKE :q
        ";
    }

    /*if ($q != ''){
        $sql .= "HAVING authors LIKE :q";
    }*/

    $stmt = $db-> prepare($sql);
    if ($q != ''){
        $stmt->bindValue(':q', "%$q%");
    }
    $stmt->execute();

}catch (PDOException $e) {
    echo "Errore: " . $e->getMessage();
    die();
}

?>

<h1>La Biblioteca</h1>

<form>
    <label for="q">cerca per <input name="q" id="q" value="<?= $q ?>"></label>
    <button>Q</button>
    <button onclick="this.form.q=''">X</button>
    <!-- <input type="button" onclick="this.form.q='';this.form.submit()" value="X"></input> -->
</form>
<br>
<table>
    <tr>
        <th>titolo <button></button></th>
        <th>autori</th>
        <th>genre</th>
        <th>anno</th>
        <th>prezzo</th>
    </tr>
    <?php while($row = $stmt->fetch(PDO::FETCH_ASSOC)): ?>
        <tr>
            <td><?= $row['title'] ?></td>
            <td><?= $row['authors'] ?></td>
            <td><?= $row['genre'] ?></td>
            <td><?= $row['year'] ?></td>
            <td><?= $row['price'] ?></td>
        </tr>
    <?php endwhile ?>

</table>
<a href="admin/books/index.php">Vai all'amministrazione dei libri</a>
<a href="admin/genres/index.php">Vai all'amministrazione dei generi</a>
</body>
</html>